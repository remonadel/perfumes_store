<?php /*%%SmartyHeaderCode:83438008058286ea0da7283-79906979%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '740413b471bf106d44b54776d08a32178f350071' => 
    array (
      0 => '/var/www/html/prestashop/modules/appagebuilder/views/templates/hook/appagebuilder.tpl',
      1 => 1471939858,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '83438008058286ea0da7283-79906979',
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58286ea3b38ef1_04991961',
  'has_nocache_code' => false,
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58286ea3b38ef1_04991961')) {function content_58286ea3b38ef1_04991961($_smarty_tpl) {?><!-- @file modules\appagebuilder\views\templates\hook\ApRow -->
    <div id="reinsurance-block"        class="row ApRow has-bg bg-fullwidth"
	        data-bg=" #ffffff no-repeat"                style=""        >
        
                            <!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApGeneral -->
<div 
    class="ApRawHtml block">
	
                <div class="feature-box">                    <div class="fbox-icon"><a href="#"><i class="icon icons-v1"></i></a></div><div class="fbox-body"><h4>Safe Payment</h4><p>Pay with the world’s most popular and secure payment methods.</p></div></div>
    	
</div>
    </div><!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApGeneral -->
<div 
    class="ApRawHtml block">
	
                <div class="feature-box">                    <div class="fbox-icon"><a href="#"><i class="icon icons-v2"></i></a></div><div class="fbox-body"><h4>Worldwide Delivery</h4><p>With sites in 9 languages, we ship to over 200 countries & regions.</p></div></div>
    	
</div>
    </div><!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApGeneral -->
<div 
    class="ApRawHtml block">
	
                <div class="feature-box">                    <div class="fbox-icon"><a href="#"><i class="icon icons-v3"></i></a></div><div class="fbox-body"><h4>24/7 Help Center</h4><p>Round-the-clock assistance for a smooth shopping experience.</p></div></div>
    	
</div>
    </div><!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApGeneral -->
<div 
    class="ApRawHtml block">
	
                <div class="feature-box last">                    <div class="fbox-icon"><a href="#"><i class="icon icons-v4"></i></a></div><div class="fbox-body"><h4>V.I.P member</h4><p>Simply add the item to your shopping bag and proceed to</p></div></div>
    	
</div>
    </div><!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApBlog -->
<div class="block latest-blogs exclusive appagebuilder" id="blog-form_1464881497" >
	
		<h4 class="title_block">
		Latest blog
	</h4>
		<div class="block_content">	
												<!-- @file modules\appagebuilder\views\templates\hook\BlogOwlCarousel -->
<div class="row">
	<div id="carousel-2124739941" class="owl-carousel owl-theme">
							<div class="item">
									<!-- @file modules\appagebuilder\views\templates\hook\BlogItem -->
<div class="blog-container" itemscope itemtype="https://schema.org/Blog">
    <div class="left-block">
        <div class="blog-image-container image">
            <a class="blog_img_link" href="http://localhost/prestashop/blog/nullam-ullamcorper-nisl-quis-ornare-molestie-b23.html" title="Nullam ullamcorper nisl quis ornare molestie" itemprop="url">
							<img class="replace-2x img-responsive" src="http://localhost/prestashop/img/leoblog/b/23/200_135/b-blog-7.jpg" 
					 alt="Nullam ullamcorper nisl quis ornare molestie" 
					 title="Nullam ullamcorper nisl quis ornare molestie" 
					 width="200" 					  height="135"					 itemprop="image" />
			            </a>
        </div>
    </div>
    <div class="right-block">
        			<h5 class="blog-title" itemprop="name"><a href="http://localhost/prestashop/blog/nullam-ullamcorper-nisl-quis-ornare-molestie-b23.html" title="Nullam ullamcorper nisl quis ornare molestie">Nullam ullamcorper nisl quis ornare...</a></h5>
        		<div class="blog-meta">
							<span class="created">
					<i class="fa fa-clock-o"></i>
					<time class="date" datetime="2016">
						Nov		<!-- month-->
						13,	<!-- day of month -->
						2016		<!-- year -->
					</time>
				</span>
						
						
						
						
					</div>
					<div class="blog-desc" itemprop="description">
				Suspendisse posuere, diam in bibendum lobortis, turpis ipsum aliquam risus, sit amet dictum ligula lorem...
			</div>
        		<div class="blog-readmore">
			<a class="btn btn-arrow-right btn-readmore" href="http://localhost/prestashop/blog/nullam-ullamcorper-nisl-quis-ornare-molestie-b23.html">Read more</a>
		</div>
    </div>
</div>



							</div>
					<div class="item">
									<!-- @file modules\appagebuilder\views\templates\hook\BlogItem -->
<div class="blog-container" itemscope itemtype="https://schema.org/Blog">
    <div class="left-block">
        <div class="blog-image-container image">
            <a class="blog_img_link" href="http://localhost/prestashop/blog/turpis-at-eleifend-leo-mi-elit-aenean-porta-ac-sed-faucibus-b22.html" title="Turpis at eleifend leo mi elit Aenean porta ac sed faucibus" itemprop="url">
							<img class="replace-2x img-responsive" src="http://localhost/prestashop/img/leoblog/b/22/200_135/b-blog-6.jpg" 
					 alt="Turpis at eleifend leo mi elit Aenean porta ac sed faucibus" 
					 title="Turpis at eleifend leo mi elit Aenean porta ac sed faucibus" 
					 width="200" 					  height="135"					 itemprop="image" />
			            </a>
        </div>
    </div>
    <div class="right-block">
        			<h5 class="blog-title" itemprop="name"><a href="http://localhost/prestashop/blog/turpis-at-eleifend-leo-mi-elit-aenean-porta-ac-sed-faucibus-b22.html" title="Turpis at eleifend leo mi elit Aenean porta ac sed faucibus">Turpis at eleifend leo mi elit Aenean...</a></h5>
        		<div class="blog-meta">
							<span class="created">
					<i class="fa fa-clock-o"></i>
					<time class="date" datetime="2016">
						Nov		<!-- month-->
						13,	<!-- day of month -->
						2016		<!-- year -->
					</time>
				</span>
						
						
						
						
					</div>
					<div class="blog-desc" itemprop="description">
				Turpis at eleifend leo mi elit Aenean porta ac sed faucibus. Nunc urna Morbi fringilla vitae orci convallis...
			</div>
        		<div class="blog-readmore">
			<a class="btn btn-arrow-right btn-readmore" href="http://localhost/prestashop/blog/turpis-at-eleifend-leo-mi-elit-aenean-porta-ac-sed-faucibus-b22.html">Read more</a>
		</div>
    </div>
</div>



							</div>
					<div class="item">
									<!-- @file modules\appagebuilder\views\templates\hook\BlogItem -->
<div class="blog-container" itemscope itemtype="https://schema.org/Blog">
    <div class="left-block">
        <div class="blog-image-container image">
            <a class="blog_img_link" href="http://localhost/prestashop/blog/morbi-condimentum-molestie-nam-enim-odio-sodales-b21.html" title="Morbi condimentum molestie Nam enim odio sodales" itemprop="url">
							<img class="replace-2x img-responsive" src="http://localhost/prestashop/img/leoblog/b/21/200_135/b-blog-5.jpg" 
					 alt="Morbi condimentum molestie Nam enim odio sodales" 
					 title="Morbi condimentum molestie Nam enim odio sodales" 
					 width="200" 					  height="135"					 itemprop="image" />
			            </a>
        </div>
    </div>
    <div class="right-block">
        			<h5 class="blog-title" itemprop="name"><a href="http://localhost/prestashop/blog/morbi-condimentum-molestie-nam-enim-odio-sodales-b21.html" title="Morbi condimentum molestie Nam enim odio sodales">Morbi condimentum molestie Nam enim...</a></h5>
        		<div class="blog-meta">
							<span class="created">
					<i class="fa fa-clock-o"></i>
					<time class="date" datetime="2016">
						Nov		<!-- month-->
						13,	<!-- day of month -->
						2016		<!-- year -->
					</time>
				</span>
						
						
						
						
					</div>
					<div class="blog-desc" itemprop="description">
				Sed mauris Pellentesque elit Aliquam at lacus interdum nascetur elit ipsum. Enim ipsum hendrerit...
			</div>
        		<div class="blog-readmore">
			<a class="btn btn-arrow-right btn-readmore" href="http://localhost/prestashop/blog/morbi-condimentum-molestie-nam-enim-odio-sodales-b21.html">Read more</a>
		</div>
    </div>
</div>



							</div>
					<div class="item">
									<!-- @file modules\appagebuilder\views\templates\hook\BlogItem -->
<div class="blog-container" itemscope itemtype="https://schema.org/Blog">
    <div class="left-block">
        <div class="blog-image-container image">
            <a class="blog_img_link" href="http://localhost/prestashop/blog/urna-pretium-elit-mauris-cursus-curabitur-at-elit-vestibulum-b20.html" title="Urna pretium elit mauris cursus Curabitur at elit Vestibulum" itemprop="url">
							<img class="replace-2x img-responsive" src="http://localhost/prestashop/img/leoblog/b/20/200_135/b-blog-4.jpg" 
					 alt="Urna pretium elit mauris cursus Curabitur at elit Vestibulum" 
					 title="Urna pretium elit mauris cursus Curabitur at elit Vestibulum" 
					 width="200" 					  height="135"					 itemprop="image" />
			            </a>
        </div>
    </div>
    <div class="right-block">
        			<h5 class="blog-title" itemprop="name"><a href="http://localhost/prestashop/blog/urna-pretium-elit-mauris-cursus-curabitur-at-elit-vestibulum-b20.html" title="Urna pretium elit mauris cursus Curabitur at elit Vestibulum">Urna pretium elit mauris cursus...</a></h5>
        		<div class="blog-meta">
							<span class="created">
					<i class="fa fa-clock-o"></i>
					<time class="date" datetime="2016">
						Nov		<!-- month-->
						13,	<!-- day of month -->
						2016		<!-- year -->
					</time>
				</span>
						
						
						
						
					</div>
					<div class="blog-desc" itemprop="description">
				Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut...
			</div>
        		<div class="blog-readmore">
			<a class="btn btn-arrow-right btn-readmore" href="http://localhost/prestashop/blog/urna-pretium-elit-mauris-cursus-curabitur-at-elit-vestibulum-b20.html">Read more</a>
		</div>
    </div>
</div>



							</div>
					<div class="item">
									<!-- @file modules\appagebuilder\views\templates\hook\BlogItem -->
<div class="blog-container" itemscope itemtype="https://schema.org/Blog">
    <div class="left-block">
        <div class="blog-image-container image">
            <a class="blog_img_link" href="http://localhost/prestashop/blog/urna-pretium-elit-mauris-cursus-curabitur-at-elit-vestibulum-b19.html" title="Urna pretium elit mauris cursus Curabitur at elit Vestibulum" itemprop="url">
							<img class="replace-2x img-responsive" src="http://localhost/prestashop/img/leoblog/b/19/200_135/b-blog-3.jpg" 
					 alt="Urna pretium elit mauris cursus Curabitur at elit Vestibulum" 
					 title="Urna pretium elit mauris cursus Curabitur at elit Vestibulum" 
					 width="200" 					  height="135"					 itemprop="image" />
			            </a>
        </div>
    </div>
    <div class="right-block">
        			<h5 class="blog-title" itemprop="name"><a href="http://localhost/prestashop/blog/urna-pretium-elit-mauris-cursus-curabitur-at-elit-vestibulum-b19.html" title="Urna pretium elit mauris cursus Curabitur at elit Vestibulum">Urna pretium elit mauris cursus...</a></h5>
        		<div class="blog-meta">
							<span class="created">
					<i class="fa fa-clock-o"></i>
					<time class="date" datetime="2016">
						Nov		<!-- month-->
						13,	<!-- day of month -->
						2016		<!-- year -->
					</time>
				</span>
						
						
						
						
					</div>
					<div class="blog-desc" itemprop="description">
				Mi vitae magnis Fusce laoreet nibh felis porttitor laoreet Vestibulum faucibus. At Nulla id tincidunt ut...
			</div>
        		<div class="blog-readmore">
			<a class="btn btn-arrow-right btn-readmore" href="http://localhost/prestashop/blog/urna-pretium-elit-mauris-cursus-curabitur-at-elit-vestibulum-b19.html">Read more</a>
		</div>
    </div>
</div>



							</div>
					<div class="item">
									<!-- @file modules\appagebuilder\views\templates\hook\BlogItem -->
<div class="blog-container" itemscope itemtype="https://schema.org/Blog">
    <div class="left-block">
        <div class="blog-image-container image">
            <a class="blog_img_link" href="http://localhost/prestashop/blog/ipsum-cursus-vestibulum-at-interdum-vivamus-b18.html" title="Ipsum cursus vestibulum at interdum Vivamus" itemprop="url">
							<img class="replace-2x img-responsive" src="http://localhost/prestashop/img/leoblog/b/18/200_135/b-blog-2.jpg" 
					 alt="Ipsum cursus vestibulum at interdum Vivamus" 
					 title="Ipsum cursus vestibulum at interdum Vivamus" 
					 width="200" 					  height="135"					 itemprop="image" />
			            </a>
        </div>
    </div>
    <div class="right-block">
        			<h5 class="blog-title" itemprop="name"><a href="http://localhost/prestashop/blog/ipsum-cursus-vestibulum-at-interdum-vivamus-b18.html" title="Ipsum cursus vestibulum at interdum Vivamus">Ipsum cursus vestibulum at interdum...</a></h5>
        		<div class="blog-meta">
							<span class="created">
					<i class="fa fa-clock-o"></i>
					<time class="date" datetime="2016">
						Nov		<!-- month-->
						13,	<!-- day of month -->
						2016		<!-- year -->
					</time>
				</span>
						
						
						
						
					</div>
					<div class="blog-desc" itemprop="description">
				Donec tellus Nulla lorem Nullam elit id ut elit feugiat lacus. Congue eget dapibus congue tincidunt...
			</div>
        		<div class="blog-readmore">
			<a class="btn btn-arrow-right btn-readmore" href="http://localhost/prestashop/blog/ipsum-cursus-vestibulum-at-interdum-vivamus-b18.html">Read more</a>
		</div>
    </div>
</div>



							</div>
			</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('#carousel-2124739941').owlCarousel({
            items : 3,
            itemsDesktop : [1199,3],            itemsDesktopSmall : [979,3],            itemsTablet : [768,2],
            itemsMobile : [479,1],            itemsCustom : false,            singleItem : false,         // true : show only 1 item
            itemsScaleUp : false,
            slideSpeed : 200,  //  change speed when drag and drop a item
            paginationSpeed : 800, // change speed when go next page

            autoPlay : false,   // time to show each item
            stopOnHover : false,
            navigation : true,
            navigationText : ["&lsaquo;", "&rsaquo;"],

            scrollPerPage : false,
            
            pagination : false, // show bullist
            paginationNumbers : false, // show number
            
            responsive : true,
            //responsiveRefreshRate : 200,
            //responsiveBaseWidth : window,
            
            //baseClass : "owl-carousel",
            //theme : "owl-theme",
            
            lazyLoad : false,
            lazyFollow : false,  // true : go to page 7th and load all images page 1...7. false : go to page 7th and load only images of page 7th
            lazyEffect : "fade",
            
            autoHeight : false,

            //jsonPath : false,
            //jsonSuccess : false,

            //dragBeforeAnimFinish
            mouseDrag : true,
            touchDrag : true,
            
            addClassActive : true,
                        //transitionStyle : "owl_transitionStyle",
            
            //beforeUpdate : false,
            //afterUpdate : false,
            //beforeInit : false,
            //afterInit : false,
            //beforeMove : false,
            //afterMove : false,
            afterAction : SetOwlCarouselFirstLast,
            //startDragging : false,
            //afterLazyLoad: false
    

        });
});

function SetOwlCarouselFirstLast(el){
	el.find(".owl-item").removeClass("first");
	el.find(".owl-item.active").first().addClass("first");

	el.find(".owl-item").removeClass("last");
	el.find(".owl-item.active").last().addClass("last");
}
</script>
								</div>
	
</div>

    </div>
            </div>
<!-- @file modules\appagebuilder\views\templates\hook\ApRow -->
    <div        class="row footer-top ApRow has-bg bg-boxed"
	        data-bg=" no-repeat"                style="background: no-repeat;"        >
        
                            <!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApImage -->
<div id="image-form_6411724482037156" class="block logo-footer ApImage">
	
                    <img src="/prestashop/themes/ap_cake/img/modules/appagebuilder/images/logo-footer.png" class="img-responsive "
                                    title=""
            alt="Shopping Office"
	    style=" width:auto; 
			height:auto" />

            	
                    <div class='image_description'>
								<p>Duis sed odio sit amet nibh vulputate cursus a sit<br /> amet mauris. Morbi accumsan ipsum velit. <br /> Nam nec tellus a odio tincidunt auctor a ornare odio.</p>
            </div>
        </div>
<!-- @file modules\appagebuilder\views\templates\hook\ApModule -->

<!-- MODULE Block contact infos -->
<div id="block_contact_infos" class="block">
	<div>
        <h4 class="title_block">Main office</h4>
        <ul>
                        	<li>
            		My Company <br>42 Puffin street
12345 Puffinville
France            	</li>
                                    	<li>
            		<span>0123-456-789</span>
            	</li>
                                    	<li>
            		<span><a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;%73%61%6c%65%73@%79%6f%75%72%63%6f%6d%70%61%6e%79.%63%6f%6d" >&#x73;&#x61;&#x6c;&#x65;&#x73;&#x40;&#x79;&#x6f;&#x75;&#x72;&#x63;&#x6f;&#x6d;&#x70;&#x61;&#x6e;&#x79;&#x2e;&#x63;&#x6f;&#x6d;</a></span>
            	</li>
                    </ul>
    </div>
</div>
<!-- /MODULE Block contact infos -->


    </div><!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApModule -->

	<!-- Block CMS module footer -->
	<div class="footer-block block" id="block_various_links_footer">
		<h4 class="title_block">Information</h4>
		<ul class="toggle-footer list-group bullet">
							<li class="item">
					<a href="http://localhost/prestashop/prices-drop" title="Specials">
						Specials
					</a>
				</li>
									<li class="item">
				<a href="http://localhost/prestashop/new-products" title="New products">
					New products
				</a>
			</li>
										<li class="item">
					<a href="http://localhost/prestashop/best-sales" title="Best sellers">
						Best sellers
					</a>
				</li>
										<li class="item">
					<a href="http://localhost/prestashop/stores" title="Our stores">
						Our stores
					</a>
				</li>
									<li class="item">
				<a href="http://localhost/prestashop/contact-us" title="Contact us">
					Contact us
				</a>
			</li>
															<li class="item">
						<a href="http://localhost/prestashop/content/3-terms-and-conditions-of-use" title="Terms and conditions of use">
							Terms and conditions of use
						</a>
					</li>
																<li class="item">
						<a href="http://localhost/prestashop/content/4-about-us" title="About us">
							About us
						</a>
					</li>
													<li>
				<a href="http://localhost/prestashop/sitemap" title="Sitemap">
					Sitemap
				</a>
			</li>
									<li>
				<span>
					<a class="_blank" href="http://www.prestashop.com"> © 2016 - Ecommerce software by PrestaShop™ </a>
				</span>
			</li>
					</ul>
		
	</div>
	<!-- /Block CMS module footer -->


    </div><!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApBlockLink -->



<div id="blockLink-form_16857147268831355" class="ApBlockLink">
    <div class="footer-block block">
        			<h4 class="title_block">
				Our Service
			</h4>
		        <ul class="toggle-footer list-group bullet">
												<li><a href="contact-us">Contact us</a></li>
																<li><a href="stores">Our stores</a></li>
																<li><a href="sitemap">Site map</a></li>
																<li><a href="#">Customer Service</a></li>
							        </ul>
    </div>
</div>


    </div><!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApModule -->

<!-- Block myaccount module -->
<div class="footer-block block">
	<h4 class="title_block">My account</h4>
	<div class="block_content toggle-footer">
		<ul class="bullet toggle-footer list-group">
			<li><a href="http://localhost/prestashop/order-history" title="My orders" rel="nofollow">My orders</a></li>
						<li><a href="http://localhost/prestashop/credit-slip" title="My credit slips" rel="nofollow">My credit slips</a></li>
			<li><a href="http://localhost/prestashop/addresses" title="My addresses" rel="nofollow">My addresses</a></li>
			<li><a href="http://localhost/prestashop/identity" title="Manage my personal information" rel="nofollow">My personal info</a></li>
						
            		</ul>
	</div>
</div>
<!-- /Block myaccount module -->


    </div><!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApModule -->

<!-- Block Newsletter module-->
<div id="newsletter_block_left" class="footer-block block inline">
	<h4 class="title_block">Newsletter</h4>
	<div class="block_content toggle-footer">
		<span class="info-newletter">Duis sed odio sit amet nibh vulputate rsus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris</span>
		<form action="//localhost/prestashop/" method="post">
			<div class="form-group">
				<input class="inputNew form-control grey newsletter-input" id="newsletter-input" type="text" name="email" size="18" value="Enter your e-mail" />
				<button type="submit" name="submitNewsletter" class="btn button button-small">
                    <i class="fa fa-envelope"></i><span>Register</span>
                </button>
				<input type="hidden" name="action" value="0" />
			</div>
		</form>
	</div>
    
</div>
<!-- /Block Newsletter module-->


    </div>
            </div>
<!-- @file modules\appagebuilder\views\templates\hook\ApRow -->
    <div id="form_6056636157366233"        class="row footer-bottom ApRow has-bg bg-fullwidth"
	        data-bg=" #28191a no-repeat"                style=""        >
        
                            <!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="copyright col-lg-6 col-md-6 col-sm-12 col-xs-12 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApGeneral -->
<div 
    class="ApHtml block">
	
                <p>Copyright © 2016 Powered by PrestaShop. All Rights Reserved.</p><p>Developed by <a href="#">LeoTheme</a></p>
    	
</div>
    </div><!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-md-6 col-lg-6 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApImage -->
<div id="image-form_7680528115405680" class="block payment ApImage">
	
                    <a href="#" >
                <img src="/prestashop/themes/ap_cake/img/modules/appagebuilder/images/payment.png" class="img-responsive "
                                    title=""
            alt="Payment"
	    style=" width:auto; 
			height:auto" />

                </a>
            	
        </div>

    </div>
            </div>
<?php }} ?>
