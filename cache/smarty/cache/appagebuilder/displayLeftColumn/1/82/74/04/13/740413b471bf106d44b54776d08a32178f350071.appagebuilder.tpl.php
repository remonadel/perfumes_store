<?php /*%%SmartyHeaderCode:83438008058286ea0da7283-79906979%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '740413b471bf106d44b54776d08a32178f350071' => 
    array (
      0 => '/var/www/html/prestashop/modules/appagebuilder/views/templates/hook/appagebuilder.tpl',
      1 => 1471939858,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '83438008058286ea0da7283-79906979',
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58286ea1e05c79_05728192',
  'has_nocache_code' => false,
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58286ea1e05c79_05728192')) {function content_58286ea1e05c79_05728192($_smarty_tpl) {?><!-- @file modules\appagebuilder\views\templates\hook\ApRow -->
    <div        class="row ApRow "
	                        style=""        >
        
                            <!-- @file modules\appagebuilder\views\templates\hook\ApColumn -->
<div    class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sp-12 ApColumn "
		    >
                <!-- @file modules\appagebuilder\views\templates\hook\ApProductCarousel -->
<div class="block products_block exclusive appagebuilder feature-products ApProductCarousel">
	
		<h4 class="title_block">
		Feature Products
	</h4>
		<div class="block_content">	
                                                <!-- @file modules\appagebuilder\views\templates\hook\ProductOwlCarousel -->
<div class="row">
	<div id="carousel-1741818517" class="owl-carousel owl-theme owl-loading plist-sidebar">
				<div class="item first">
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="1"></div>
		<a class="product_img_link" href="http://localhost/prestashop/tshirts/1-faded-short-sleeves-tshirt.html" title="Faded Short Sleeves T-shirt" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/1-home_default/faded-short-sleeves-tshirt.jpg" alt="Faded Short Sleeves T-shirt" title="Faded Short Sleeves T-shirt"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="1"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/tshirts/1-faded-short-sleeves-tshirt.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/tshirts/1-faded-short-sleeves-tshirt.html" title="Faded Short Sleeves T-shirt" itemprop="url" >
		Faded Short...
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 18.16			</span>
						
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="2"></div>
		<a class="product_img_link" href="http://localhost/prestashop/blouses/2-blouse.html" title="Blouse" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/7-home_default/blouse.jpg" alt="Blouse" title="Blouse"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="2"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/blouses/2-blouse.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/blouses/2-blouse.html" title="Blouse" itemprop="url" >
		Blouse
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 29.70			</span>
						
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="3"></div>
		<a class="product_img_link" href="http://localhost/prestashop/casual-dresses/3-printed-dress.html" title="Printed Dress" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/8-home_default/printed-dress.jpg" alt="Printed Dress" title="Printed Dress"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="3"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/casual-dresses/3-printed-dress.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/casual-dresses/3-printed-dress.html" title="Printed Dress" itemprop="url" >
		Printed Dress
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 28.60			</span>
						
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="4"></div>
		<a class="product_img_link" href="http://localhost/prestashop/evening-dresses/4-printed-dress.html" title="Printed Dress" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/10-home_default/printed-dress.jpg" alt="Printed Dress" title="Printed Dress"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="4"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/evening-dresses/4-printed-dress.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/evening-dresses/4-printed-dress.html" title="Printed Dress" itemprop="url" >
		Printed Dress
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 56.09			</span>
						
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="5"></div>
		<a class="product_img_link" href="http://localhost/prestashop/summer-dresses/5-printed-summer-dress.html" title="Printed Summer Dress" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/12-home_default/printed-summer-dress.jpg" alt="Printed Summer Dress" title="Printed Summer Dress"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="5"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/summer-dresses/5-printed-summer-dress.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/summer-dresses/5-printed-summer-dress.html" title="Printed Summer Dress" itemprop="url" >
		Printed Summer Dress
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 31.88			</span>
							
				<span class="old-price product-price">
					£ 33.56
				</span>
				
									<span class="price-percent-reduction">-5%</span>
										
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="6"></div>
		<a class="product_img_link" href="http://localhost/prestashop/summer-dresses/6-printed-summer-dress.html" title="Printed Summer Dress" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/16-home_default/printed-summer-dress.jpg" alt="Printed Summer Dress" title="Printed Summer Dress"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="6"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/summer-dresses/6-printed-summer-dress.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/summer-dresses/6-printed-summer-dress.html" title="Printed Summer Dress" itemprop="url" >
		Printed Summer Dress
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 33.55			</span>
						
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
					</div>
			<div class="item">
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="7"></div>
		<a class="product_img_link" href="http://localhost/prestashop/summer-dresses/7-printed-chiffon-dress.html" title="Printed Chiffon Dress" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/20-home_default/printed-chiffon-dress.jpg" alt="Printed Chiffon Dress" title="Printed Chiffon Dress"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="7"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/summer-dresses/7-printed-chiffon-dress.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/summer-dresses/7-printed-chiffon-dress.html" title="Printed Chiffon Dress" itemprop="url" >
		Printed Chiffon...
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 18.04			</span>
							
				<span class="old-price product-price">
					£ 22.55
				</span>
				
									<span class="price-percent-reduction">-20%</span>
										
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
					</div>
		</div>
</div>




<script type="text/javascript">
$(window).load(function(){
	$('#carousel-1741818517').owlCarousel({
            items : 1,
            itemsDesktop : [1199,1],            itemsDesktopSmall : [979,1],            itemsTablet : [768,1],
            itemsMobile : [479,1],            itemsCustom : false,            singleItem : false,         // true : show only 1 item
            itemsScaleUp : false,
            slideSpeed : 200,  //  change speed when drag and drop a item
            paginationSpeed : 800, // change speed when go next page

            autoPlay : false,   // time to show each item
            stopOnHover : false,
            navigation : false,
            navigationText : ["&lsaquo;", "&rsaquo;"],

            scrollPerPage : false,
            
            pagination : false, // show bullist
            paginationNumbers : false, // show number
            
            responsive : true,
            //responsiveRefreshRate : 200,
            //responsiveBaseWidth : window,
            
            //baseClass : "owl-carousel",
            //theme : "owl-theme",
            
            lazyLoad : false,
            lazyFollow : false,  // true : go to page 7th and load all images page 1...7. false : go to page 7th and load only images of page 7th
            lazyEffect : "fade",
            
            autoHeight : false,

            //jsonPath : false,
            //jsonSuccess : false,

            //dragBeforeAnimFinish
            mouseDrag : true,
            touchDrag : true,
            
            addClassActive : true,
                        //transitionStyle : "owl_transitionStyle",
            
            //beforeUpdate : false,
            //afterUpdate : false,
            //beforeInit : false,
            afterInit: OwlLoaded,
            //beforeMove : false,
            //afterMove : false,
            afterAction : SetOwlCarouselFirstLast,
            //startDragging : false,
            //afterLazyLoad: false
    

        });
});
function OwlLoaded(el){
	el.removeClass('owl-loading').addClass('owl-loaded');
};

function SetOwlCarouselFirstLast(el){
	el.find(".owl-item").removeClass("first");
	el.find(".owl-item.active").first().addClass("first");

	el.find(".owl-item").removeClass("last");
	el.find(".owl-item.active").last().addClass("last");
}
</script>
                            	</div>
	
</div><!-- @file modules\appagebuilder\views\templates\hook\ApImage -->
<div id="image-form_18682274234880775" class="block img_anim ApImage">
	
                    <a href="#" >
                <img src="/prestashop/themes/ap_cake/img/modules/appagebuilder/images/img1-home6.jpg" class="img-responsive "
                                    title=""
            alt="Best Photocopy"
	    style=" width:auto; 
			height:auto" />

                </a>
            	
        </div>
<!-- @file modules\appagebuilder\views\templates\hook\ApProductCarousel -->
<div class="block products_block exclusive appagebuilder sale-off margin-bt45 ApProductCarousel">
	
		<h4 class="title_block">
		Sale Off
	</h4>
		<div class="block_content">	
                                                <!-- @file modules\appagebuilder\views\templates\hook\ProductOwlCarousel -->
<div class="row">
	<div id="carousel-3592512129" class="owl-carousel owl-theme owl-loading plist-sidebar">
				<div class="item first">
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="1"></div>
		<a class="product_img_link" href="http://localhost/prestashop/tshirts/1-faded-short-sleeves-tshirt.html" title="Faded Short Sleeves T-shirt" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/1-home_default/faded-short-sleeves-tshirt.jpg" alt="Faded Short Sleeves T-shirt" title="Faded Short Sleeves T-shirt"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="1"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/tshirts/1-faded-short-sleeves-tshirt.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/tshirts/1-faded-short-sleeves-tshirt.html" title="Faded Short Sleeves T-shirt" itemprop="url" >
		Faded Short...
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 18.16			</span>
						
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="2"></div>
		<a class="product_img_link" href="http://localhost/prestashop/blouses/2-blouse.html" title="Blouse" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/7-home_default/blouse.jpg" alt="Blouse" title="Blouse"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="2"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/blouses/2-blouse.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/blouses/2-blouse.html" title="Blouse" itemprop="url" >
		Blouse
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 29.70			</span>
						
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="3"></div>
		<a class="product_img_link" href="http://localhost/prestashop/casual-dresses/3-printed-dress.html" title="Printed Dress" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/8-home_default/printed-dress.jpg" alt="Printed Dress" title="Printed Dress"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="3"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/casual-dresses/3-printed-dress.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/casual-dresses/3-printed-dress.html" title="Printed Dress" itemprop="url" >
		Printed Dress
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 28.60			</span>
						
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
					</div>
			<div class="item">
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="4"></div>
		<a class="product_img_link" href="http://localhost/prestashop/evening-dresses/4-printed-dress.html" title="Printed Dress" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/10-home_default/printed-dress.jpg" alt="Printed Dress" title="Printed Dress"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="4"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/evening-dresses/4-printed-dress.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/evening-dresses/4-printed-dress.html" title="Printed Dress" itemprop="url" >
		Printed Dress
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 56.09			</span>
						
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="5"></div>
		<a class="product_img_link" href="http://localhost/prestashop/summer-dresses/5-printed-summer-dress.html" title="Printed Summer Dress" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/12-home_default/printed-summer-dress.jpg" alt="Printed Summer Dress" title="Printed Summer Dress"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="5"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/summer-dresses/5-printed-summer-dress.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/summer-dresses/5-printed-summer-dress.html" title="Printed Summer Dress" itemprop="url" >
		Printed Summer Dress
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 31.88			</span>
							
				<span class="old-price product-price">
					£ 33.56
				</span>
				
									<span class="price-percent-reduction">-5%</span>
										
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
																	<div class="product-container product-block" itemscope itemtype="http://schema.org/Product"><div class="left-block">
<!-- @file modules\appagebuilder\views\templates\front\products\image_container -->
<div class="product-image-container image">
	<div class="leo-more-info hidden-xs" data-idproduct="6"></div>
		<a class="product_img_link" href="http://localhost/prestashop/summer-dresses/6-printed-summer-dress.html" title="Printed Summer Dress" itemprop="url">
			<img class="replace-2x img-responsive" src="http://localhost/prestashop/16-home_default/printed-summer-dress.jpg" alt="Printed Summer Dress" title="Printed Summer Dress"  width="320" height="384" itemprop="image" />
			<span class="product-additional" data-idproduct="6"></span>
		</a>
					<a class="new-box" href="http://localhost/prestashop/summer-dresses/6-printed-summer-dress.html">
				<span class="label-new product-label label-info label">New</span>
			</a>
				</div>


</div><div class="right-block"><div class="product-meta">
<!-- @file modules\appagebuilder\views\templates\front\products\name -->
<h5 itemprop="name" class="name">
		<a class="product-name" href="http://localhost/prestashop/summer-dresses/6-printed-summer-dress.html" title="Printed Summer Dress" itemprop="url" >
		Printed Summer Dress
	</a>
</h5>


<div class="product-info">
<!-- @file modules\appagebuilder\views\templates\front\products\price -->
	<div class="content_price">
					
			<span class="price product-price">
				£ 33.55			</span>
						
			
			
			</div>
<!-- @file modules\appagebuilder\views\templates\front\products\reviews -->
		
</div></div></div></div>
				
					</div>
		</div>
</div>




<script type="text/javascript">
$(window).load(function(){
	$('#carousel-3592512129').owlCarousel({
            items : 1,
            itemsDesktop : [1199,1],            itemsDesktopSmall : [979,1],            itemsTablet : [768,1],
            itemsMobile : [479,1],            itemsCustom : false,            singleItem : false,         // true : show only 1 item
            itemsScaleUp : false,
            slideSpeed : 200,  //  change speed when drag and drop a item
            paginationSpeed : 800, // change speed when go next page

            autoPlay : false,   // time to show each item
            stopOnHover : false,
            navigation : false,
            navigationText : ["&lsaquo;", "&rsaquo;"],

            scrollPerPage : false,
            
            pagination : false, // show bullist
            paginationNumbers : false, // show number
            
            responsive : true,
            //responsiveRefreshRate : 200,
            //responsiveBaseWidth : window,
            
            //baseClass : "owl-carousel",
            //theme : "owl-theme",
            
            lazyLoad : false,
            lazyFollow : false,  // true : go to page 7th and load all images page 1...7. false : go to page 7th and load only images of page 7th
            lazyEffect : "fade",
            
            autoHeight : false,

            //jsonPath : false,
            //jsonSuccess : false,

            //dragBeforeAnimFinish
            mouseDrag : true,
            touchDrag : true,
            
            addClassActive : true,
                        //transitionStyle : "owl_transitionStyle",
            
            //beforeUpdate : false,
            //afterUpdate : false,
            //beforeInit : false,
            afterInit: OwlLoaded,
            //beforeMove : false,
            //afterMove : false,
            afterAction : SetOwlCarouselFirstLast,
            //startDragging : false,
            //afterLazyLoad: false
    

        });
});
function OwlLoaded(el){
	el.removeClass('owl-loading').addClass('owl-loaded');
};

function SetOwlCarouselFirstLast(el){
	el.find(".owl-item").removeClass("first");
	el.find(".owl-item.active").first().addClass("first");

	el.find(".owl-item").removeClass("last");
	el.find(".owl-item.active").last().addClass("last");
}
</script>
                            	</div>
	
</div>
    </div>
            </div>
<?php }} ?>
