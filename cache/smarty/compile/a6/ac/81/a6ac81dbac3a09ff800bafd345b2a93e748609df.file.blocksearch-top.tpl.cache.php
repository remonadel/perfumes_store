<?php /* Smarty version Smarty-3.1.19, created on 2016-11-13 15:46:08
         compiled from "/var/www/html/prestashop/themes/ap_cake/modules/blocksearch/blocksearch-top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:48656484858286ea0607c52-49999700%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6ac81dbac3a09ff800bafd345b2a93e748609df' => 
    array (
      0 => '/var/www/html/prestashop/themes/ap_cake/modules/blocksearch/blocksearch-top.tpl',
      1 => 1471939826,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '48656484858286ea0607c52-49999700',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'search_query' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58286ea060dc58_02918222',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58286ea060dc58_02918222')) {function content_58286ea060dc58_02918222($_smarty_tpl) {?>
<!-- Block search module TOP -->
<div class="block-search">
	<div class="btn-group search-focus" data-toggle="dropdown">
		<a href="#"><i class="fa fa-search"></i></a>
	</div>
	<div id="search_block_top" class="nav-search visible">
		<form id="searchbox" method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search',null,null,null,false,null,true), ENT_QUOTES, 'UTF-8', true);?>
" >
			<input type="hidden" name="controller" value="search" />
			<input type="hidden" name="orderby" value="position" />
			<input type="hidden" name="orderway" value="desc" />
			<input class="search_query form-control" type="text" id="search_query_top" name="search_query" placeholder="<?php echo smartyTranslate(array('s'=>'Search...','mod'=>'blocksearch'),$_smarty_tpl);?>
" value="<?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_query']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
" />
			<button type="submit" name="submit_search" class="button-search fa fa-search"></button> 
		</form>
		<span class="button-close">
			<i class="fa fa-close"></i>
		</span>
	</div>
</div>
<!-- /Block search module TOP --><?php }} ?>
