<?php /* Smarty version Smarty-3.1.19, created on 2016-11-13 15:46:10
         compiled from "/var/www/html/prestashop/themes/ap_cake/modules/appagebuilder/views/templates/hook/ApCategoryImage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:199901391858286ea217ca47-08045488%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '54d9e8d0bf43f27cacd31dca69b61e9e617f3f44' => 
    array (
      0 => '/var/www/html/prestashop/themes/ap_cake/modules/appagebuilder/views/templates/hook/ApCategoryImage.tpl',
      1 => 1471939820,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '199901391858286ea217ca47-08045488',
  'function' => 
  array (
    'menu' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'level' => 0,
    'random' => 0,
    'data' => 0,
    'category' => 0,
    'link' => 0,
    'formAtts' => 0,
    'categories' => 0,
    'apLiveEdit' => 0,
    'cate' => 0,
    'apLiveEditEnd' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58286ea21cdaf6_54522934',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58286ea21cdaf6_54522934')) {function content_58286ea21cdaf6_54522934($_smarty_tpl) {?>
<!-- @file modules\appagebuilder\views\templates\hook\ApCategoryImage -->
<?php if (!function_exists('smarty_template_function_menu')) {
    function smarty_template_function_menu($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['menu']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
<ul class="level<?php echo intval($_smarty_tpl->tpl_vars['level']->value);?>
 <?php if ($_smarty_tpl->tpl_vars['level']->value==0) {?> ul-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['random']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">

<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
	
	<?php if (isset($_smarty_tpl->tpl_vars['category']->value['children'])&&is_array($_smarty_tpl->tpl_vars['category']->value['children'])) {?>
	<li class="cate_<?php echo intval($_smarty_tpl->tpl_vars['category']->value['id_category']);?>
">
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['category']->value['id_category'],$_smarty_tpl->tpl_vars['category']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true);?>
">
			<span class="cate_content">
				<span class="cover-img">
					<?php if (isset($_smarty_tpl->tpl_vars['category']->value['image'])) {?>
					<img class="img-responsive" src='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value["image"], ENT_QUOTES, 'UTF-8', true);?>
' alt='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
' 
						<?php if ($_smarty_tpl->tpl_vars['formAtts']->value['showicons']==0||($_smarty_tpl->tpl_vars['level']->value>0&&$_smarty_tpl->tpl_vars['formAtts']->value['showicons']==2)) {?> style="display:none"<?php }?>/>
					<?php }?>
				</span>
				<span class="<?php if ($_smarty_tpl->tpl_vars['level']->value==0) {?>cate-parent <?php }?>cate-name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</span>
			</span>
		</a>
		<?php smarty_template_function_menu($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['category']->value['children'],'level'=>$_smarty_tpl->tpl_vars['level']->value+1));?>

		
	</li>
	<?php } else { ?>
	
	<li class="cate_<?php echo intval($_smarty_tpl->tpl_vars['category']->value['id_category']);?>
">
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['category']->value['id_category'],$_smarty_tpl->tpl_vars['category']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true);?>
">
			<span class="cate_content">
				<span class="cover-img">
					<?php if (isset($_smarty_tpl->tpl_vars['category']->value['image'])) {?>
					<img class="img-responsive" src='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value["image"], ENT_QUOTES, 'UTF-8', true);?>
' alt='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value["name"], ENT_QUOTES, 'UTF-8', true);?>
' 
						<?php if ($_smarty_tpl->tpl_vars['formAtts']->value['showicons']==0||($_smarty_tpl->tpl_vars['level']->value>0&&$_smarty_tpl->tpl_vars['formAtts']->value['showicons']==2)) {?> style="display:none"<?php }?>/>
					<?php }?>
				</span>
				
				<?php if ($_smarty_tpl->tpl_vars['formAtts']->value['cate_depth']==1) {?>
					<span class="nbproducts-child">
						<span class="name-cat"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</span>
						<span id="leo-cat-<?php echo $_smarty_tpl->tpl_vars['category']->value['id_category'];?>
" class="leo-qty" data-str="<?php echo smartyTranslate(array('s'=>'products','mod'=>'appagebuilder'),$_smarty_tpl);?>
"></span>
					</span>
				<?php } else { ?>
					<span class="name-cat"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</span>
				<?php }?>
			</span>
		</a>
	</li>
	<?php }?>
<?php } ?>
</ul>
<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>


<?php if (isset($_smarty_tpl->tpl_vars['categories']->value)) {?>
<div class="widget-category_image block">
	<?php echo $_smarty_tpl->tpl_vars['apLiveEdit']->value ? $_smarty_tpl->tpl_vars['apLiveEdit']->value : '';?>

	<?php if (isset($_smarty_tpl->tpl_vars['formAtts']->value['title'])&&!empty($_smarty_tpl->tpl_vars['formAtts']->value['title'])) {?>
	<h4 class="title_block">
		<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['formAtts']->value['title'], ENT_QUOTES, 'UTF-8', true);?>

	</h4>
	<?php }?>
	<div class="block_content">
		<?php  $_smarty_tpl->tpl_vars['cate'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cate']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cate']->key => $_smarty_tpl->tpl_vars['cate']->value) {
$_smarty_tpl->tpl_vars['cate']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['cate']->key;
?>
			<?php smarty_template_function_menu($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['cate']->value));?>

		<?php } ?>
		<div id="view_all_wapper_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['random']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="view_all_wapper">
			<a class ="view_all" href="javascript:void(0)"><?php echo smartyTranslate(array('s'=>'View all','mod'=>'appagebuilder'),$_smarty_tpl);?>
</a>
		</div> 
	</div>
	<?php echo $_smarty_tpl->tpl_vars['apLiveEditEnd']->value ? $_smarty_tpl->tpl_vars['apLiveEditEnd']->value : '';?>

</div>
<?php }?>
<script type="text/javascript">
 
	jQuery(document).ready(function() {
		$(".view_all_wapper").hide();
		var limit = <?php echo intval($_smarty_tpl->tpl_vars['formAtts']->value['limit']);?>
;
		var level = <?php echo intval($_smarty_tpl->tpl_vars['formAtts']->value['cate_depth']);?>
 - 1;
		$("ul.ul-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['random']->value, ENT_QUOTES, 'UTF-8', true);?>
, ul.ul-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['random']->value, ENT_QUOTES, 'UTF-8', true);?>
 ul").each(function(){
			var element = $(this).find(">li").length;
			var count = 0;
			$(this).find(">li").each(function(){
				count = count + 1;
				if(count > limit){
					// $(this).remove();
					$(this).hide();
				}
			});
			if(element > limit) {
				view = $(".view_all","#view_all_wapper_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['random']->value, ENT_QUOTES, 'UTF-8', true);?>
").clone(1);
				// view.appendTo($(this).find("ul.level" + level));
				view.appendTo($(this));
				var href = $(this).closest("li").find('a:first-child').attr('href');
				$(view).attr("href", href);
			}
		})
	});

</script><?php }} ?>
